//-----------------------------------------------------------------------------
// File:    ska-talondx-i2c-app.c
// Version: 1.2.0
// Date:    Jan 23, 2024
// Author:  Richard Hein (richard.hein@mda.space)
//
// Description: 
//
// Utility to check and query key information of all Talon-DX I2C devices.
//
// to compile: aarch64-linux-gnu-gcc src/ska-talondx-i2c-app.c -Wall -o ska-talondx-i2c-app
// to copy: scp ska-talondx-i2c-app talon4:/bin
// to run with make: make run target=talon1
//
// Revision History:
//
// Version: 1.0.0
// Date:    Jul 13, 2023
// - Initial version
//
// Version: 1.1.0
// Date:    Nov 6, 2023
// - Moved to BIST repo
//
// Version: 1.2.0
// Date:    Jan 23, 2024
// - Added additional verbose output information
// - Cleanup of output display
// 
// Version: 1.2.1
// Date:    July 10, 2024
// - Added exit codes to temperature check funtion for use with other scripts calling this application
//
// Version: 1.3.0
// Date:    Apr 9, 2024
// - Added DC current values
//
// Version: 1.4.0
// Date:    Jun 13, 2024
// - Show FPGA temperatures before second I2C bus reading
// 
// Version: 1.5.0
// Date:    Aug 16, 2024
// - Enable fan controller configuration and monitoring
// - DC power calculation
// - Ambient temperature check
//
// Version: 1.5.1
// Date:    Aug 26, 2024
// - Adjusted profile calulation for fan RPM failure
//
// Version: 1.5.2
// Date:    Sep 6, 2024
// - Changed expected 0.85V power supplies to 0.9V
//
// Version: 1.6.0
// Date:    Oct 31, 2024
// - Report XCVR configuration state
// - Option to flash front panel LEDs
//
//-----------------------------------------------------------------------------

#define VERSION "1.6.0" // for BIST 0.1.0

// Configure and check fan status if this file is found
#define FAN_DETECT_FILE "/home/root/ska-talondx-i2c-app.cfg"

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <signal.h>

// I2C Access
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

// HPS GPIO Access
#include <linux/gpio.h>

// FPGA Access
#include <sys/mman.h>

// From talon_dx-tdc_base-tdc_bist.json:
// "TOP|E_BASE|G_100GBE[x]|E_100GBE|G_QSFP_CTRL|E_QSFP", firmware_offset
#define QSFP_CTRL_0 512
#define QSFP_CTRL_1 520

// From ska-mid-cbf-talondx-bist-0.1.0.tar.gz/talon_dx-tdc_base-tdc_bist.json
// "TOP|E_BASE|E_PERSONA|E_LED_CTRL|E_CSR", firmware_offset
#define LED_CTRL 0

//-----------------------------------------------------------------------------
// HPS GPIO Access

#define DEV_NAME0 "/dev/gpiochip0"
#define DEV_NAME1 "/dev/gpiochip1"

int read_gpio(int fd, uint32_t line_number){

    struct gpiochip_info info;
    struct gpiohandle_request rq;
    struct gpiohandle_data data;

    int ret = ioctl(fd, GPIO_GET_CHIPINFO_IOCTL, &info);
    if (ret == -1)
    {
        printf("Unable to get chip info from ioctl: %s\n", strerror(errno));
        close(fd);
        return -1;
    }

    printf("Number of lines: %d\n", info.lines);

    if( line_number > info.lines ){
        printf("ERROR: The gpiochip has only %d lines\n", info.lines);
        return -3;
    }

    rq.lines = 1; //we read 1 line at a time
    rq.flags = GPIOHANDLE_REQUEST_INPUT;
    rq.lineoffsets[0] = line_number;

    ret = ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &rq);

    (void)close(fd);

    if (ret == -1)
    {
        printf("Unable to get line handle from ioctl : %s\n", strerror(errno));
        return -4;
    }
    
    ret = ioctl(rq.fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data);

    if (ret == -1)
    {
        printf("Unable to get line value using ioctl : %s\n", strerror(errno));
    }else
    {
        printf("gpio_line: %u value = %u\n", line_number, data.values[0]);
    }

    close(rq.fd);

    return 0;
}
 
int gpio_write(int fd, uint32_t line_number, unsigned char val){

    struct gpiohandle_request rq;
    struct gpiohandle_data data;
    
    rq.lines = 1; //we read 1 line at a time
    rq.flags = GPIOHANDLE_REQUEST_OUTPUT;
    rq.lineoffsets[0] = line_number;

    int ret = ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &rq);

    (void)close(fd);

    if (ret == -1)
    {
        printf("Unable to get line handle from ioctl : %s\n", strerror(errno));
        return -4;
    }

    data.values[0] = val;

    ret = ioctl(rq.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);

    if (ret == -1)
    {
        printf("Unable to set line value using ioctl : %s", strerror(errno));
    } else {
        //printf("gpio_line: %u value = %u written\n", line_number, data.values[0]);
    }

    close(rq.fd);

    return 0;
}
 
int hps_gpio_write(int dev_num, uint32_t line_number, unsigned char gpio_val) {
      
    int fd, ret;
    
    switch (dev_num) {
        case 0: fd = open(DEV_NAME0, O_RDONLY); break;
        case 1: fd = open(DEV_NAME1, O_RDONLY); break;
        default: fd = -1;
    }

    if (fd < 0)
    {
        printf("Unable to open DEV_NAME%d: %s\n", dev_num, strerror(errno));
        return -1;
    }
    
    ret = gpio_write(fd, line_number, gpio_val);
    
    return ret;
}

//-----------------------------------------------------------------------------
// FPGA Register Access

#define c_base_addr_lwsoc2fpga    0xF9000000
#define c_mem_map_size            0x200000   // 2MB memory space

static int fd_mem = -1;                          // File descriptor for memory mapped object
static void *bridge_map;                     //memory memory address

// Functions
void initLWHPS2FPGA();
void cleanupLWHPS2FPGA();
void writeLWHPS2FPGA(unsigned offset, unsigned data);
unsigned readLWHPS2FPGA(unsigned offset);

void fpga_write (unsigned addr, unsigned value) {

   initLWHPS2FPGA();
   writeLWHPS2FPGA(addr, value);
   cleanupLWHPS2FPGA();
}

void initLWHPS2FPGA()
{
    off_t bridge_base = c_base_addr_lwsoc2fpga;

    /* open the memory device file */
    fd_mem = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd_mem < 0) {
        perror("open /dev/mem");
        exit(EXIT_FAILURE);
    }

    /* map the LWHPS2FPGA bridge into process memory */
    bridge_map = mmap(NULL, c_mem_map_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_mem, bridge_base);
    if (bridge_map == MAP_FAILED) {
        perror("mmap for lwhps2fpga");
        close(fd_mem);
        exit(EXIT_FAILURE);
    }
}

void cleanupLWHPS2FPGA()
{
    if (munmap(bridge_map, c_mem_map_size) < 0)
        perror("munmap");

    if (fd_mem >= 0)
        close(fd_mem);
}

void writeLWHPS2FPGA(unsigned offset, unsigned data)
{
    volatile unsigned *reg;

    reg = bridge_map + offset;
    *reg = data;
}

unsigned readLWHPS2FPGA(unsigned offset)
{
    volatile unsigned *reg;

    reg = bridge_map + offset;

    return *reg;
}

//-----------------------------------------------------------------------------
// I2C SMB Access

// https://www.kernel.org/doc/html/latest/i2c/smbus-protocol.html
// https://github.com/omapconf/omapconf/blob/40ab0a2a57dffcf7e2813fc7a526d6cde3755347/i2c-tools/i2c-dev.h
// https://github.com/omapconf/omapconf/blob/40ab0a2a57dffcf7e2813fc7a526d6cde3755347/i2c-tools/i2cget.c
// https://github.com/omapconf/omapconf/blob/40ab0a2a57dffcf7e2813fc7a526d6cde3755347/i2c-tools/i2cset.c

static inline __s32 i2c_smbus_access(int file, char read_write, __u8 command, int size, union i2c_smbus_data *data)
{
	struct i2c_smbus_ioctl_data args;

	args.read_write = read_write;
	args.command = command;
	args.size = size;
	args.data = data;
	return ioctl(file, I2C_SMBUS, &args);
}

static inline __s32 i2c_smbus_write_quick(int file, __u8 value)
{
	return i2c_smbus_access(file, value, 0, I2C_SMBUS_QUICK, NULL);
}

static inline __s32 i2c_smbus_read_byte(int file)
{
	union i2c_smbus_data data;
	if (i2c_smbus_access(file, I2C_SMBUS_READ, 0, I2C_SMBUS_BYTE, &data))
		return -1;
	else
		return 0x0FF & data.byte;
}

static inline __s32 i2c_smbus_write_byte(int file, __u8 value)
{
	return i2c_smbus_access(file, I2C_SMBUS_WRITE, value, I2C_SMBUS_BYTE, NULL);
}

static inline __s32 i2c_smbus_read_byte_data(int file, __u8 command)
{
	union i2c_smbus_data data;
	if (i2c_smbus_access(file, I2C_SMBUS_READ, command, I2C_SMBUS_BYTE_DATA, &data))
		return -1;
	else
		return 0x0FF & data.byte;
}

static inline __s32 i2c_smbus_write_byte_data(int file, __u8 command, __u8 value)
{
	union i2c_smbus_data data;
	data.byte = value;
	return i2c_smbus_access(file, I2C_SMBUS_WRITE, command, I2C_SMBUS_BYTE_DATA, &data);
}

static inline __s32 i2c_smbus_read_word_data(int file, __u8 command)
{
	union i2c_smbus_data data;
	if (i2c_smbus_access(file, I2C_SMBUS_READ, command, I2C_SMBUS_WORD_DATA, &data))
		return -1;
	else
		return 0x0FFFF & data.word;
}

static inline __s32 i2c_smbus_write_word_data(int file, __u8 command, __u16 value)
{
	union i2c_smbus_data data;
	data.word = value;
	return i2c_smbus_access(file, I2C_SMBUS_WRITE, command, I2C_SMBUS_WORD_DATA, &data);
}

static inline __s32 i2c_smbus_process_call(int file, __u8 command, __u16 value)
{
	union i2c_smbus_data data;
	data.word = value;
	if (i2c_smbus_access(file, I2C_SMBUS_WRITE, command, I2C_SMBUS_PROC_CALL, &data))
		return -1;
	else
		return 0x0FFFF & data.word;
}

/* Returns the number of read bytes */
static inline __s32 i2c_smbus_read_block_data(int file, __u8 command, __u8 * values)
{
	union i2c_smbus_data data;
	if (i2c_smbus_access(file, I2C_SMBUS_READ, command, I2C_SMBUS_BLOCK_DATA, &data))
		return -1;
	else {
		for (int i = 1; i <= data.block[0]; i++)
			values[i - 1] = data.block[i];
		return data.block[0];
	}
}

static inline __s32 i2c_smbus_write_block_data(int file, __u8 command, __u8 length, const __u8 * values)
{
	union i2c_smbus_data data;
	int i;
	if (length > 32)
		length = 32;
	for (i = 1; i <= length; i++)
		data.block[i] = values[i - 1];
	data.block[0] = length;
	return i2c_smbus_access(file, I2C_SMBUS_WRITE, command, I2C_SMBUS_BLOCK_DATA, &data);
}

/* Returns the number of read bytes */
/* Until kernel 2.6.22, the length is hardcoded to 32 bytes. If you
   ask for less than 32 bytes, your code will only work with kernels
   2.6.23 and later. */
static inline __s32 i2c_smbus_read_i2c_block_data(int file, __u8 command, __u8 length, __u8 * values)
{
	union i2c_smbus_data data;

	if (length > 32)
		length = 32;
	data.block[0] = length;
	if (i2c_smbus_access(file, I2C_SMBUS_READ, command, length == 32 ? I2C_SMBUS_I2C_BLOCK_BROKEN : I2C_SMBUS_I2C_BLOCK_DATA, &data))
		return -1;
	else {
		for (int i = 1; i <= data.block[0]; i++)
			values[i - 1] = data.block[i];
		return data.block[0];
	}
}

static inline __s32 i2c_smbus_write_i2c_block_data(int file, __u8 command, __u8 length, const __u8 * values)
{
	union i2c_smbus_data data;
	int i;
	if (length > 32)
		length = 32;
	for (i = 1; i <= length; i++)
		data.block[i] = values[i - 1];
	data.block[0] = length;
	return i2c_smbus_access(file, I2C_SMBUS_WRITE, command, I2C_SMBUS_I2C_BLOCK_BROKEN, &data);
}

//-----------------------------------------------------------------------------
// I2C Device Access

int i2c_open_bus_device(int bus, int addr) {
    int file;
    char filename[40];
    
    // open I2C bus
    sprintf(filename, "/dev/i2c-%d", bus);
    if ((file = open(filename,O_RDWR)) < 0) {
        printf("I²C Bus %d - Failed to open I²C bus, FPGA most likely not programmed\n", bus);
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    // initiate connection to I2C device
    if (ioctl(file, I2C_SLAVE_FORCE, addr) < 0) {
        printf("I²C Bus %d - Failed to acquire bus access and/or talk to slave.\n", bus);
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    return file;
}

void i2c_wr_byte(int bus, int address, __u8 daddress, char *name_str, int byte)
{
    int file;
    
    file = i2c_open_bus_device(bus, address);
    
    if (i2c_smbus_write_byte_data(file, daddress, byte) <0 ){
        printf("%s: Error - Unable to write byte data\n", name_str);
    }

    close(file);
}

int i2c_rd_word(int bus, int address, __u8 daddress)
{
    int file;
    int word;
    
    file = i2c_open_bus_device(bus, address);
    word = i2c_smbus_read_word_data(file, daddress);
    close(file);
    return word;
}

int i2c_rd_word_check(int bus, int address, __u8 daddress, char *name_str, char *format_str)
{
    int word;
    char str[80];
    
    sprintf(str, "%s: %s\n", name_str, format_str);
    
    word = i2c_rd_word(bus, address, daddress);

    if (word < 0) {
        printf("%s: Not Found\n", name_str);    
    } else {
        printf(str, word);    
    }
    return word;
}

int i2c_rd_byte(int bus, int address, __u8 daddress)
{
    int file;
    int byte;
    
    file = i2c_open_bus_device(bus, address);
    byte = i2c_smbus_read_byte_data(file, daddress);
    close(file);
    return byte;
}

int i2c_rd_byte_check(int bus, int address, __u8 daddress, char *name_str, char *format_str)
{
    int byte;
    char str[80];
    
    printf("[%d,0x%02X] ", bus, address);
    
    sprintf(str, "%s %s", name_str, format_str);
    
    byte = i2c_rd_byte(bus, address, daddress);
    
    if (byte < 0) {
        printf("%s Not Found\n", name_str);    
    } else {
        printf(str, byte);    
    }
    return byte;
}


void i2c_rd_ascii(int bus, int address, __u8 daddress, int length, char *format_str)
{
    int file;
    int byte;
    __u8 values[32];
    
    for (int i=0; i < 32; i++) {
        values[i] = 0;
    }
    
    file = i2c_open_bus_device(bus, address);
    
    byte = i2c_smbus_read_i2c_block_data(file, daddress, length, values);
    
    if (byte < 0) {
        printf("Not Found\n");    
    } else {
        // trim
        int i=31;
        while (i >= 0 && values[i]==32) {
            values[i] = 0;
            i--;
        }
        printf(format_str, values);
    }
    close(file);
}

//-----------------------------------------------------------------------------
// Device Access

#define WARNING_TEMP 100
#define ERROR_TEMP   101
#define OK_TEMP      0

int temperature_check (char *label, float t, float max, bool short_form) {
    
    const float ambient = 20.0;
    const int range = 10;
    int exit_code;
    
    float t_step = (max - ambient) / (float)range;
    
    if (short_form) {
        printf("%0.1f°C ", t);
    } else {
        printf("%0.0f°C|", ambient);
        for (int i = 0; i <= range; i++) {
            if (ambient+t_step*(i-0.5f) < t && t <= ambient+t_step*(i+0.5f)) {
                printf("*");
            } else {
                printf("-");
            }
        }
        printf("|%0.0f°C %0.1f°C %s ", max, t, label);
    }
    
    if (t > max) {
        printf("Failure: Over Temperature");
        exit_code = ERROR_TEMP;
    } else if (t > max - 0.1*(max-ambient)) {
        printf("Warning: Close to Maximum");
        exit_code = WARNING_TEMP;
    } else {
        printf("Ok");
        exit_code = OK_TEMP;
    }
    
    if (!short_form) {
        printf("\n");
    }
    
    return exit_code;
}

int twos_complement (int value, int size) {
    int mask = (1 << size) - 1;
    int val = value & mask;
    if (val >= (1 << (size-1))) {
        return val - (1 << size);
    } else {
        return val;
    }
}

int dimm_temp_decode(int address, char *name_str) {

    // DIMM temperature
    // DIMM F0 0x1C  0b0011_100 temp
    // i2cget -f -y 1 0x1C 5 w
    // temperature-sensing solution based on system requirements and JEDEC JC-42.2
    // https://www.nxp.com/docs/en/data-sheet/SE97B.pdf
    // https://www.analog.com/media/en/technical-documentation/data-sheets/ADT7408.pdf   
    // SMBBus / I2C - JEDEC standard JC-42.4 Mobile Platform Memory Module Temperature Sensor Component specification
    // SCL 10-100 kHz, 0 - 400 kHz
    
    int file;
    int word;
    int tempi;
    float tempf;
    int exit_code = 0;
    
    printf("/ [%d,0x%02X] ", 1, address);
    
    file = i2c_open_bus_device(1, address);
    word = i2c_smbus_read_word_data(file, 5);    
    close(file);
    //printf("Word: 0x%04X\n", word);    
    /*
    if (word & 0x0080) {
        printf("ACT - Above Critical Trip, ");
    }
    if (word & 0x0040) {
        printf("AAW - Above Alarm Window, ");
    }
    if (word & 0x0020) {
        printf("BAW - Below Alarm Window, ");
    }
    */
    tempi = ((word << 8) | (word >> 8)) & 0x0FFF;
    
    printf("%s: ", name_str);
    if (word & 0x0010) {
        // negative
        tempf = (tempi-4096) / 16.0;
    } else {
        // positive
        tempf = tempi / 16.0;
    }
    //printf("%.2f°C\n", tempf);
    exit_code |= temperature_check("", tempf, 85.0f, false);

    return exit_code;
}

// 0, 0x40 - Temperature & Humidity
int check_temperature_humidity(bool is_verbose) {

    // Temperature sensor
    // U26 - Si7013_A20-GM
    // https://www.silabs.com/documents/public/data-sheets/Si7013-A20.pdf
    // 0x40 0b100_0000
    // i2ctransfer -f -y 0 w1@0x40 0xE3 r2@0x40
    // i2cget -f -y 0 0x40 0xE3 w
    
    int address = 0x40;
    int file;
    int word;
    int temp_code;
    int rh_code;
    
    file = i2c_open_bus_device(0, address);
    word = i2c_smbus_read_word_data(file, 0xE3);
    temp_code = ((word << 8) | (word >> 8)) & 0xFFFF;
    word = i2c_smbus_read_word_data(file, 0xE5);
    rh_code = ((word << 8) | (word >> 8)) & 0xFFFF;
    close(file);
    
    float temperature = 175.72 * temp_code / 65536.0 - 46.85;
    float humidity = 125.0 * rh_code / 65536.0 - 6.0;
    
    printf("[%d,0x%02X] ", 0, address);
    printf("Humidity: %.1f%%, ", humidity);
    printf("Temperature: ");
    int exit_code = temperature_check("", temperature, 45.0f, false);
    
    return exit_code;
}

float linear11_decode(int bus, int address, __u8 daddress, char *format_str) {
    
    // LTM4676A - https://www.analog.com/media/en/technical-documentation/data-sheets/4676afa.pdf
    // LTM4677A - https://www.analog.com/media/en/technical-documentation/data-sheets/ltm4677.pdf
    // PMBus Linear11 (Linear_5s_11s) format
    // https://embeddedartistry.com/blog/2018/07/05/linear11-conversion-using-a-sign-extension-bit-twiddling-hack/ 
    // V = X * 2^N where 
    //  V is the decimal value corresponding to the LINEAR11 data
    //  X is a signed 11-bit 2’s complement integer
    //  N is a signed 5-bit 2’s compliment integer
    
    int file;
    int word;
    
    file = i2c_open_bus_device(bus, address);
    word = i2c_smbus_read_word_data(file, daddress);    
    close(file);
    
    int exponent = twos_complement(word >> 11, 5);
    int mantissa = twos_complement(word, 11);
    
    float pow;
    if (exponent < 0) {
        pow = 1.0f / (float)(1 << -exponent);
    } else {
        pow = (float)(1 << exponent);
    }
    float temp = pow * (float)mantissa;
        
    printf(format_str, temp);
    
    return temp;
}

float linear11_decode_paged(int bus, int page, int address, __u8 daddress, char *format_str) {
    
    // LTM4676A - https://www.analog.com/media/en/technical-documentation/data-sheets/4676afa.pdf
    // LTM4677A - https://www.analog.com/media/en/technical-documentation/data-sheets/ltm4677.pdf
    // PMBus Linear11 (Linear_5s_11s) format
    // https://embeddedartistry.com/blog/2018/07/05/linear11-conversion-using-a-sign-extension-bit-twiddling-hack/ 
    // V = X * 2^N where 
    //  V is the decimal value corresponding to the LINEAR11 data
    //  X is a signed 11-bit 2’s complement integer
    //  N is a signed 5-bit 2’s compliment integer
    
    int file;
    int word;
    
    file = i2c_open_bus_device(bus, address);
    if (i2c_smbus_write_byte_data(file, 0x00, page) < 0){
        printf("Error - Unable to write byte data\n");
        close(file);
        return -1.0;
    }
    word = i2c_smbus_read_word_data(file, daddress);    
    close(file);
    
    int exponent = twos_complement(word >> 11, 5);
    int mantissa = twos_complement(word, 11);
    
    float pow;
    if (exponent < 0) {
        pow = 1.0f / (float)(1 << -exponent);
    } else {
        pow = (float)(1 << exponent);
    }
    float temp = pow * (float)mantissa;
        
    printf(format_str, temp);
    
    return temp;
}

float linear16_decode_paged(int bus, int page, int address, __u8 daddress, char *format_str) {
    
    // LTM4676A - https://www.analog.com/media/en/technical-documentation/data-sheets/4676afa.pdf
    // LTM4677A - https://www.analog.com/media/en/technical-documentation/data-sheets/ltm4677.pdf
    // Power Voltage
    // PMBus Linear16 (Linear_16u) format
    // https://www.monolithicpower.com/en/digital-communication-in-power-supply-applications
    // VOUT_MODE (0x20) = 20 = 0x14 = 0b000_10100 => (2^-12)
    // V = reg * 2^-12
    
    int file;
    int word;
    
    file = i2c_open_bus_device(bus, address);
    if (i2c_smbus_write_byte_data(file, 0x00, page) < 0){
        printf("Error - Unable to write byte data\n");
        close(file);
        return -1.0;
    }
    word = i2c_smbus_read_word_data(file, daddress);    
    close(file);
    
    int mantissa = twos_complement(word, 16);
    
    float pow = 1.0f / (float)(1 << 12);
    float temp = pow * (float)mantissa;
        
    printf(format_str, temp);
    
    return temp;
}

void dimm_spd_decode(int address, char *name_str, bool is_verbose) {
    
    // http://www.softnology.biz/pdf/DDR4_SPD_4_01_02_12R23A_Jan2014.pdf
    // https://www.st.com/resource/en/datasheet/m34e04.pdf
    int byte;
    int addr;
    int file;
    
    file = i2c_open_bus_device(1, address);
    
    printf("[%d,0x%02X] %s", 1, address, name_str);

    // Number of Bytes Used
    //byte = i2c_smbus_read_byte_data(file, 0);
    //printf(" 0=0x%02X, Number of Bytes Used: ", byte);
    //printf("%d\n", (byte & 0xF) * 128);
    
    // SPD Revision
    //byte = i2c_smbus_read_byte_data(file, 1);
    //printf(" 1=0x%02X, SPD Revision: ", byte);
    //printf("%d.%d\n", (byte & 0xF0) >> 4, (byte & 0x0F));
    
    // DRAM Device Type
    byte = i2c_smbus_read_byte_data(file, 2);
    if (byte == 0x0C) {
        printf("DDR4 SDRAM, ");
    } else {
        printf("Unknown 0x%02X, ", byte);
    }
    
    // Module Type
    byte = i2c_smbus_read_byte_data(file, 3);
    //printf(" 3=0x%02X, Module Type: ", byte);
    if (byte == 0x01) {
        printf("RDIMM,  ");
    } else if (byte == 0x04) {
        printf("LRDIMM, ");
    } else {
        printf("Unknown 0x%02X, ", byte);
    }
    
    byte = i2c_smbus_read_byte_data(file, 18);
    switch (byte) {
        case 0x05: printf("DDR4-3200 (1600 MHz clock), "); break;
        case 0x06: printf("DDR4-2666 (1333 MHz clock), "); break;
        case 0x07: printf("DDR4-2400 (1200 MHz clock), "); break;
        case 0x08: printf("DDR4-2133 (1067 MHz clock), "); break;
        case 0x09: printf("DDR4-1866 (933  MHz clock), "); break;
        case 0x0A: printf("DDR4-1600 (800  MHz clock), "); break;
        default:   printf("DDR4-Unknown 0x%02X, ", byte);
    }
    
    int ranks_per_dimm = 2; // 2 package ranks per DIMM
    int sdram_capacity = 2; // 2 Gb per die
    int sdram_width = 4; // x4 organization
    int primary_bus_width = 64; // 64 bit primary bus
    
    // SDRAM Density and Banks
    byte = i2c_smbus_read_byte_data(file, 4); // 0x86, 10000110, 4 bank groups, 4 banks, 16Gb
    //printf(" 4=0x%02X, SDRAM Density and Banks: ", byte);
    // SDRAM Capacity = SPD byte 4 bits 3~0 = 16 Gb
    sdram_capacity = (1 << (byte & 0x0F)) / 4;
    //printf("SDRAM capacity = %dGb\n", sdram_capacity);
    
    // SDRAM Package Type
    byte = i2c_smbus_read_byte_data(file, 6); // 0xB2, 1.011.00.10, 3DS, 4 die, 3DS
    //printf(" 6=0x%02X, SDRAM Package Type: ", byte);
    // for 3DS: = SPD byte 12 bits 5~3 times SPD byte 6 bits 6~4
    int die_count = ((byte >> 4) & 0x7) + 1;
    //printf("Die Count = %d\n", die_count);
    
    // Module Organization
    byte = i2c_smbus_read_byte_data(file, 12); // 0x08, 00.001.000, 2 package ranks, 4 bit device width
    //printf(" 12=0x%02X, Module Organization: ", byte);
    // SDRAM Width = SPD byte 12 bits 2~0 = 4
    sdram_width = 4 << (byte & 0x7);
    // Logical Ranks per DIMM = SPD byte 12 bits 5~3 = 2
    ranks_per_dimm = ((byte >> 3) & 0x7) + 1;
    //printf("SDRAM Device Width = %d, Package Ranks per DIMM = %d\n", sdram_width, ranks_per_dimm);
    
    // Module Memory Bus Width
    byte = i2c_smbus_read_byte_data(file, 13); // 0x0B, 000.01.011, 8 bit extension, 64 bits, 
    //printf(" 13=0x%02X, Module Memory Bus Width: ", byte);
    // Primary Bus Width = SPD byte 13 bits 2~0 = 64
    primary_bus_width = 8 << (byte & 0x7);
    //printf("Primary bus width = %d\n", primary_bus_width);
    
    //  2 Gb / 8 * 64 / 4 * 2 * 1 = 8 GB
    int total = sdram_capacity * primary_bus_width * ranks_per_dimm * die_count / sdram_width / 8;
    
    printf("%3d GB ", total);
    
    close(file);
    
    if (!is_verbose) return;
    
    // 0x36, 0x37
    file = i2c_open_bus_device(1, 0x37); // Select upper page
    //i2c_smbus_write_quick(file, 1); // Select upper 256 bits
    i2c_smbus_read_byte_data(file, 0x0);
    close(file);
    
    // Module Part Number
    file = i2c_open_bus_device(1, address);
    printf("\n  Part Number: ");
    // Bytes 329~348 (0x149~15C): Module Part Number
    for (addr = 0x149; addr <= 0x15C; addr++) {
        byte = i2c_smbus_read_byte_data(file, addr);
        printf("%c", byte);
    }
    printf("\n  Serial ID:   ");
    
    // Serial Identifier
    // 1: Byte 320 (0x140): Module Manufacturer ID Code, LSB      80 (40)
    // 1: Byte 321 (0x141): Module Manufacturer ID Code, MSB      CE (69)
    // 1: Byte 322 (0x142): Module Manufacturing Location         01 (00)
    // 2: Bytes 323~324 (0x143~0x144): Module Manufacturing Date  19 44 (09 44)
    // 4: Bytes 325~328 (0x145~0x148): Module Serial Number       03 A8 E0 65
    for (addr = 0x140; addr <= 0x148; addr++) {
        byte = i2c_smbus_read_byte_data(file, addr);
        printf("%02X", byte);
    }
    printf("\n  ");
    
    close(file);
    
    // 0x36, 0x37
    file = i2c_open_bus_device(1, 0x36); // Restore lower page
    //i2c_smbus_write_quick(file, 1); // Select upper 256 bits
    i2c_smbus_read_byte_data(file, 0x0);
    close(file);
}

bool is_fan_controller() {
    
   FILE *fp = fopen(FAN_DETECT_FILE, "r");
   
   if (fp) {
      fclose(fp);
      return true;
   } else {
      return false;
   }
}

void configure_fan_controller(int file) {
    
    // Fan Configuration - Mode:PWM, Spin-Up:1s, Control, TACH Input Enabled
    for (int fan_number=0; fan_number < 4; fan_number++) {
        i2c_smbus_write_byte_data(file, 0x02+fan_number, 0x48); // 0100 1000
    }
    // Fan Fault Mask
    i2c_smbus_write_byte_data(file, 0x13, 0x30);
    // Fan Fail Options: assert fan failure on 1st fail detected
    i2c_smbus_write_byte_data(file, 0x14, 0x44);
    // cleared by writing a PWM target duty cycle
    // TACH target count
    for (int fan_number=0; fan_number < 4; fan_number++) {
        i2c_smbus_write_byte_data(file, 0x50+fan_number*2, 0x3C);
        i2c_smbus_write_byte_data(file, 0x51+fan_number*2, 0x00);
    }
    // wait 2s for fan state to be determined
    sleep(2);
}

void set_fan_speed(int file, int fan_number, int percent) {
    int pwm = 511 * percent / 100;
    i2c_smbus_write_byte_data(file, 0x40+fan_number*2, (pwm >> 1) & 0xFF);
    i2c_smbus_write_byte_data(file, 0x41+fan_number*2, (pwm << 7) & 0xFF);
}

// 0, 0x20 - Fan Controller
int check_fan_controller(bool is_verbose) {
    // fan_number is 0 to 3
    // 
    // Sanyo Denki - 9GA0812P1G61, 10500 RPM 100%, 2000 RPM 0% talon8
    // Orion - 0D8038-12HBVXC10A, 6300 RPM 100%, 1700 RPM 0%
    // 
    // MAX31790
    // https://www.analog.com/media/en/technical-documentation/data-sheets/MAX31790.pdf
    // i2cget -f -y 0 0x20 0x50
    // fan_status=`i2cget -f -y 0 0x20 0x11`

    int exit_code = 0;
    
    printf("[%d,0x%02X] Fan Controller", 0, 0x20);

    int file = i2c_open_bus_device(0, 0x20);
    
    int status = i2c_smbus_read_byte_data(file, 0x11); // Fan Fault Status 1
    
    if (status < 0) {
        printf(", Access failure\n");
    } else if (!is_fan_controller()) {
        printf(" - Fans controlled by other Talon\n");
    } else {
        
        // check if fan monitoring enabled
        bool tach_input_enable = true;
        for (int fan_number=0; fan_number < 4; fan_number++) {
            int byte = i2c_smbus_read_byte_data(file, 0x02+fan_number); // Fan n Dynamics
            if ((byte & 0x08) == 0) { // Bit 3 TACH Input Enable
                tach_input_enable = false;
            }
        }
        if (!tach_input_enable) {
            printf(", incorrect setting - fan speed not monitored - configuring fan controller...\n");
            configure_fan_controller(file);
            status = i2c_smbus_read_byte_data(file, 0x11); // Fan Fault Status 1
        }
        
        if (is_verbose) {
            printf("\n");
        }
        for (int fan_number=0; fan_number < 4; fan_number++) {
            
            int fan_failure = (status >> fan_number) & 1;
            
            int byte = i2c_smbus_read_byte_data(file, 0x08+fan_number); // Fan n Dynamics
            int sr;
            switch (byte >> 5) {
                case 0: sr = 1; break;
                case 1: sr = 2; break;
                case 2: sr = 4; break;
                case 3: sr = 8; break;
                case 4: sr = 16; break;
                default:sr = 32;
            }
            
            int tach_count_msb = i2c_smbus_read_byte_data(file, 0x18+fan_number*2); // TACH n Count MSB
            int tach_count_lsb = i2c_smbus_read_byte_data(file, 0x19+fan_number*2); // TACH n Count LSB
            int tach_count = (tach_count_msb << 3) | (tach_count_lsb >> 5);
            
            int pwm_duty_cycle_msb = i2c_smbus_read_byte_data(file, 0x30+fan_number*2); // PWMOUT n Duty Cycle MSB
            int pwm_duty_cycle_lsb = i2c_smbus_read_byte_data(file, 0x31+fan_number*2); // PWMOUT n Duty Cycle LSB
            int pwm_duty_cycle = (pwm_duty_cycle_msb << 1) | (pwm_duty_cycle_lsb >> 7);
            
            float rpm = 30.0 * (float)sr * 8192.0 / (float)tach_count;
            float pwm = 100.0 * (float)pwm_duty_cycle / 511.0;
            
            #define FAN_RPM_FAILURE_LIMIT 1500
            // Orion:   1700 (10%) -> 6300  (100%) - 0D8038-12HBVXC10A (talon15)
            #define FAN_OR_RPM_MIN 1700
            #define FAN_OR_RPM_MAX 6300
            // San Ace: 2000 (10%) -> 10500 (100%) - 9GA0812P1G61 (talon8)
            #define FAN_SA_RPM_MIN 2000
            #define FAN_SA_RPM_MAX 10500
            
            bool fan_rpm_failure;
            if (pwm <= 10.0) {
                // expect RPM between 1700-10% and 2000+10%
                fan_rpm_failure = (FAN_OR_RPM_MIN*0.9 > rpm || rpm > FAN_SA_RPM_MIN*1.1);
            } else {
                float rpm_calc;
                
                // 1700 (10%) -> 6300 (100%) Orion - 0D8038-12HBVXC10A (talon15)
                rpm_calc = FAN_OR_RPM_MIN + pwm * (FAN_OR_RPM_MAX - FAN_OR_RPM_MIN) / 100.0f;
                bool fan_or_rpm_failure = (fabsf(rpm - rpm_calc) > FAN_RPM_FAILURE_LIMIT);

                // 2000 (10%) -> 10500 (100%) San Ace - 9GA0812P1G61 (talon8)
                rpm_calc = FAN_SA_RPM_MIN + pwm * (FAN_SA_RPM_MAX - FAN_SA_RPM_MIN) / 100.0f;
                bool fan_sa_rpm_failure = (fabsf(rpm - rpm_calc) > FAN_RPM_FAILURE_LIMIT);
                
                fan_rpm_failure = fan_or_rpm_failure && fan_sa_rpm_failure;
            }
            
            if (fan_failure || fan_rpm_failure) {
                exit_code |= ERROR_TEMP;
            }
            
            if (is_verbose) {
                // fan per line
                printf("  Fan %d: RPM= %7.1f, PWM= %.0f%%, ", fan_number+1, rpm, pwm);
                if (fan_failure) {
                    printf("Failure\n");    
                } else if (fan_rpm_failure) {
                    printf("RPM Problem\n");    
                } else {
                    printf("Ok\n");    
                }
            } else {
                // all fans on one line
                printf(", Fan%d: PWM %.0f%%", fan_number+1, pwm);
                if (fan_failure) {
                    printf(" Failure");    
                } else if (fan_rpm_failure) {
                    printf(" RPM Problem");
                } else {
                    printf(" Ok");    
                }
            }
        }
        if (!is_verbose) {
            printf("\n");
        }
    }

    close(file);
    
    return exit_code;
}

//-----------------------------------------------------------------------------

// 0, 0x33 - ADC
void check_adc(bool is_verbose) {
    i2c_rd_byte_check(0, 0x33, 0, "ADC", "Register 0: %02X\n");
}

int check_mbo_register(int address, __u8 daddress, int byte_expected) {
    
    int byte = i2c_rd_byte(0, address, daddress);
    
    if (byte != byte_expected) {
        return 1;
    } else {
        return 0;
    }
}

int check_mbo_config(int tx_addr) {
    
    int rx_addr = tx_addr & 0x4F;
    int exit_errors = 0;
    
    // Rx Global CDR = On
    exit_errors += check_mbo_register(rx_addr, 43, 0x01);
    // Rx Output Amplitude Control = 4
    exit_errors += check_mbo_register(rx_addr, 62, 0x88);
    exit_errors += check_mbo_register(rx_addr, 63, 0x88);
    exit_errors += check_mbo_register(rx_addr, 64, 0x88);
    exit_errors += check_mbo_register(rx_addr, 65, 0x88);
    exit_errors += check_mbo_register(rx_addr, 66, 0x88);
    exit_errors += check_mbo_register(rx_addr, 67, 0x88);
    // Rx Output De-emphasis Control = 0
    exit_errors += check_mbo_register(rx_addr, 68, 0x00);
    exit_errors += check_mbo_register(rx_addr, 69, 0x00);
    exit_errors += check_mbo_register(rx_addr, 70, 0x00);
    exit_errors += check_mbo_register(rx_addr, 71, 0x00);
    exit_errors += check_mbo_register(rx_addr, 72, 0x00);
    exit_errors += check_mbo_register(rx_addr, 73, 0x00);
    // Tx Global CDR = On
    exit_errors += check_mbo_register(tx_addr, 43, 0x01);
    // Tx Input High Equalization = 5
    exit_errors += check_mbo_register(tx_addr, 62, 0x55);
    exit_errors += check_mbo_register(tx_addr, 63, 0x55);
    exit_errors += check_mbo_register(tx_addr, 64, 0x55);
    exit_errors += check_mbo_register(tx_addr, 65, 0x55);
    exit_errors += check_mbo_register(tx_addr, 66, 0x55);
    exit_errors += check_mbo_register(tx_addr, 67, 0x55);
    // Tx Input Mid Equalization = 10
    exit_errors += check_mbo_register(tx_addr, 68, 0xAA);
    exit_errors += check_mbo_register(tx_addr, 69, 0xAA);
    exit_errors += check_mbo_register(tx_addr, 70, 0xAA);
    exit_errors += check_mbo_register(tx_addr, 71, 0xAA);
    exit_errors += check_mbo_register(tx_addr, 72, 0xAA);
    exit_errors += check_mbo_register(tx_addr, 73, 0xAA);
    
    return exit_errors;
}

int check_mbo(int tx_addr, char *name_str) {
    
    float t;
    int exit_code = 0;
    char str[40];
    
    int errors = check_mbo_config(tx_addr);
    if (errors) {
        sprintf(str, "%s not configured (%d),", name_str, errors); 
    } else {
        sprintf(str, "%s configured,", name_str); 
    }
    
    t = i2c_rd_byte_check(0, tx_addr, 22, str, "Temperature: ");
    if (t > 0.0f) {
        exit_code = temperature_check("", t, 70.0f, false);
    }
    
    return exit_code;
}

// 0, MBOs
int check_mbos(bool is_verbose) {
    
    int exit_code = 0;
    
    // Leap transceivers
    exit_code |= check_mbo(0x56, "Leap 1");
    exit_code |= check_mbo(0x54, "Leap 2");
    exit_code |= check_mbo(0x53, "Leap 3");
    exit_code |= check_mbo(0x55, "Leap 4");
    exit_code |= check_mbo(0x57, "Leap 5");

    return exit_code;
}

// 0, 0x50 - EEPROM
void check_eeprom(bool is_verbose) {
    
    struct s_eeprom {
        char pad[128];
        char version[128];
        char part_number[128];
        char revision[128];
        char serial_number[128];
        char manufacture_date[128];
        char mac_address[128];
    } eeprom;
    
    printf("[%d,0x%02X] EEPROM ", 0, 0x50);
    
    int byte = i2c_rd_byte(0, 0x50, 0);
    if (byte < 0) {
        printf("Error Not Found\n");
        return;
    }
   
    int fd = open("/sys/bus/i2c/devices/i2c-0/0-0050/eeprom", O_RDONLY);
    if (fd < 0){
        printf("Error: unable to open EEPROM data\n");
        return;
    }

    read(fd, &eeprom, sizeof(eeprom));
    
    // check if unconfigured
    bool is_blank = true;
    char *ptr = (char*)&eeprom;
    for (int i=0; i<sizeof(eeprom); i++) {
        if (*ptr != 0xFF) {
            is_blank = false;
            break;
        }
        ptr++;
    }
    
    if (is_blank) {
        printf("Error: EEPROM not configured\n");
    } else {
        printf("Ver: %s, ",  eeprom.version);
        printf("PN: %s, ",   eeprom.part_number);
        printf("Rev: %s, ",  eeprom.revision);
        printf("SN: %s, ",   eeprom.serial_number);
        printf("Date: %s, ", eeprom.manufacture_date);
        printf("MAC: %s\n",  eeprom.mac_address);
    }
    
    close(fd); 
}

// 1, 0x54-0x57 DIMM SPD, 0x1C-0x1F DIMM Temperature
int check_dimm(bool is_verbose) {

    int exit_code = 0;

    dimm_spd_decode(0x54, "DIMM FO ", is_verbose);
    exit_code |= dimm_temp_decode(0x1C, "Temperature");
    
    dimm_spd_decode(0x55, "DIMM F1 ", is_verbose);
    exit_code |= dimm_temp_decode(0x1D, "Temperature");
    
    dimm_spd_decode(0x56, "DIMM F2 ", is_verbose);
    exit_code |= dimm_temp_decode(0x1E, "Temperature");
    
    dimm_spd_decode(0x57, "DIMM F3 ", is_verbose);
    exit_code |= dimm_temp_decode(0x1F, "Temperature");
    
    return exit_code;
}

// QSFP modules
void check_qsfp(bool is_verbose) {
    // Standards: IEEE 802.3bm, QSFP28 MSA, SFF-8665, 
    // DAC: IEEE 802.3,SFP28 MSA, SFF-8665
    // QSFP28 or later with SFF-8636 management interface (SFF-8665 et al.) *2
    
    // SFF-8636 Rev 2.11 January 03, 2023 - Bulk of I2C registers
    // SFF-8024 - Main I2C registers
    
    // SFF-8024 Transceiver Management - for types of QSFP definition
    //   Transceiver Management section of SFF-8024
    // Table 4-1 Identifier Values
    //   SFF-8636 and CMIS Page 00h Byte 0 and Page 00h Byte 128
    // Table 4-2 Encoding Values
    //   SFF-8436, SFF-8636 and CMIS Page 00h Byte 139
    // Table 4-3 Connector Types
    //   SFF-8436, SFF-8636 and CMIS Page 00h Byte 130
    // Table 4-4 Extended Specification Compliance Codes
    //   SFF-8636 and CMIS Page 00h Byte 192
    
    const unsigned qsfp_ctrl[2] = {QSFP_CTRL_0, QSFP_CTRL_1};
    
    for (int i=0; i <= 1; i++) {
        printf("[%d,0x%02X] 100G QSFP %d ", 1, 0x50, i);

        fpga_write(qsfp_ctrl[i], 2);
        
        // Physical Device Identifier
        int byte = i2c_rd_byte(1, 0x50, 128); // 0x11 = QSFP28
        
        if (byte < 0) {
            printf("Not Found\n");
        } else if (byte != 0x11) {
            printf("Unknown 0x%02X\n", byte);
        } else {
            byte = i2c_rd_byte(1, 0x50, 192); // Extended Specification Compliance Codes (SFF-8024 Table 4-4)
            switch (byte) {
                case 0x02: printf("Type: 100GBASE-SR4, "); break;
                case 0x03: printf("Type: 100GBASE-LR4, "); break;
                case 0x0B: printf("Type: 100GBASE-CR4, "); break;
                default:   printf("Type: Unknown 0x%02X, ", byte);
            }
            // Vendor Name (00h 148-163)
            i2c_rd_ascii(1, 0x50, 148, 16, "Vendor: %s, ");
            // Vendor Part Number (00h 168-183)
            i2c_rd_ascii(1, 0x50, 168, 16, "PN: %s, ");
            // Vendor Serial Number (00h 196-211)
            i2c_rd_ascii(1, 0x50, 196, 16, "SN: %s\n");
        }
        
        fpga_write(qsfp_ctrl[i], 3);
    }
}

// GbE SFP Modules
// 1, 0x50, 0x51 (v5)
// 1, 0x72, 0x73 (v6)
void check_sfp(int addr) {

    // Bus 2, Ch 0 = #6
    // Bus 3, Ch 1 = #5
    // Bus 4, Ch 2 = #4
    // Bus 5, Ch 3 = #3
    // Bus 6, Ch 4 = #2
    // Bus 7, Ch 5 = #1
    // Bus 8, Ch 6 = #GbE2
    // Bus 9, Ch 7 = #GbE1
    
    for (int b=9; b >= 8; b--) {
        printf("[%d,0x%02X] GbE SFP %d ", b, addr, 9-b);

        // Physical Device Identifier
        int byte = i2c_rd_byte(b, addr, 0); // 0x03 = SFP/SFP+/SFP28
        
        if (byte < 0) {
            printf("Not Found\n");
        } else if (byte != 0x03) {
            printf("Unknown 0x%02X\n", byte);
        } else {
            // Vendor name [Address A0h, Bytes 20-35]
            i2c_rd_ascii(b, addr, 20, 16, "Vendor: %s, ");
            // Vendor PN [Address A0h, Bytes 40-55]
            i2c_rd_ascii(b, addr, 40, 16, "PN: %s, ");
            // Vendor SN [Address A0h, Bytes 68-83]
            i2c_rd_ascii(b, addr, 68, 16, "SN: %s\n");
        }
    }

}

void set_led_state(int state) {
    hps_gpio_write(0, 6 , state & 1); // (GPIO0_IO6)  led2(0) - HPS LED red
    //hps_gpio_write(1, 18, 1);       // (GPIO1_IO18) led2(1) - HPS LED green
    fpga_write(LED_CTRL, state);
}

/* Signal Handler for SIGINT Ctrl-C*/
void sigint_handler(int sig_num)
{
    set_led_state(0x00);
    exit(0);
}

void flash_leds() {

    // use power supply capability 0xB0 as check for bus 1 access
    int byte = i2c_rd_byte(1, 0x40, 0x19);
    if (byte != 0xB0) {
        printf(" FPGA not programmed\n");
        exit(0);
    }

    printf("flashing front panel LEDs, Ctrl-C to exit\n");
    while (1) {
        set_led_state(0x55); // Red    01010101
        sleep(1);
        set_led_state(0xFF); // Yellow 11111111
        sleep(1);
        set_led_state(0xAA); // Green  10101010
        sleep(1);
        set_led_state(0x00); // Off    00000000
        sleep(1);
    }
}

// Power Supplies
//    Upper, Lower
// 1, 0x59,  0x5B (v5): Power Supplies
// 1, 0x6D,  0x6F (v6): Power Supplies
int check_ac_power(int addr, char *name_str, bool is_verbose) {
    // PFE850-12-054NA
    // https://www.belfuse.com/product/part-details?partn=PFE850-12-054NA
    // PMBus
    
    int byte;
    float tempf;
    float v, i;
    int exit_code = 0;
    
    printf("[%d,0x%02X] %s ", 1, addr, name_str);

    //hps_gpio_write(0, 6 , 1); // (GPIO0_IO6)  led2(0) - HPS LED red
    
    // enable I2C to power supplies
    hps_gpio_write(1, 23, 1); // (GPIO1_IO23 B28) atx_i2c_en

    byte = i2c_rd_byte(1, addr, 0);
    
    if (byte < 0) {
        printf("Not Found\n");
    } else if (!is_verbose) {
        linear11_decode(1, addr, 0x88, "Vin: %06.2f V"); // Belfuse-1 Controller, Linear Format, N = -1
        linear11_decode(1, addr, 0x8B, ", Vout: %05.2f V"); // Belfuse-1 Controller, Linear Format, N = -6
        linear11_decode(1, addr, 0x8C, ", Iout: %05.2f A"); // Belfuse-1 Controller, Linear Format, N = -3
        tempf = linear11_decode(1, addr, 0x8D, ", Temp1: "); // Belfuse-0 Controller, Linear Format, N = -3
        exit_code |= temperature_check("", tempf, 45.0f, true);
        tempf = linear11_decode(1, addr, 0x8E, ", Temp2: "); // Belfuse-0 Controller, Linear Format, N = -3
        exit_code |= temperature_check("", tempf, 45.0f, true);
        printf("\n");
    } else {
        printf("\n");
        // V5: 0x51, 0x53
        // V6: 0x65, 0x67
        
        //linear11_decode(1, addr, 0xE0, "  Belfuse-0 D0D3h 3.3V: %0.2f\n");
        //linear11_decode(1, addr, 0xE1, "  Belfuse-0 D140h 5.0V: %0.2f\n");
        //linear11_decode(1, addr, 0xE2, "  Belfuse-0 E827h 5.0A: %0.2f\n");
        //linear11_decode(1, addr, 0xE3, "  Belfuse-0 0807h 15W : %0.2f\n");
        
        //i2c_rd_byte_check(1, 0x51^addr_xor, 0, "Belfuse-0 EEPROM", ": %02X\n"); // Belfuse-0 EEPROM
        //i2c_rd_byte_check(1, 0x53^addr_xor, 0, "Belfuse-1 EEPROM", ": %02X\n"); // Belfuse-1 EEPROM
        // i2cget -y 1 0x6D 0x02
        // V5: 0x59, 0x5B
        // V6: 0x6D, 0x6F

        v = linear11_decode(1, addr, 0x88, "  Vin: %0.2f V\n"); // Belfuse-1 Controller, Linear Format, N = -1
        i = linear11_decode(1, addr, 0x89, "  Iin: %0.2f A\n"); // Belfuse-1 Controller, Linear Format, N = -6
        printf("  Pin: %0.1f W\n", i*v);
        v = linear11_decode(1, addr, 0x8B, "  Vout: %0.2f V\n"); // Belfuse-1 Controller, Linear Format, N = -6
        i = linear11_decode(1, addr, 0x8C, "  Iout: %0.2f A\n"); // Belfuse-1 Controller, Linear Format, N = -3
        printf("  Pout: %0.1f W\n", i*v);
        tempf = linear11_decode(1, addr, 0x8D, "  Temperature1: "); // Belfuse-0 Controller, Linear Format, N = -3
        exit_code |= temperature_check("", tempf, 45.0f, false);
        tempf = linear11_decode(1, addr, 0x8E, "  Temperature2: "); // Belfuse-0 Controller, Linear Format, N = -3
        exit_code |= temperature_check("", tempf, 45.0f, false);
        linear11_decode(1, addr, 0x90, "  Fan Speed: %0.1f RPM\n"); // Belfuse-0 Controller, Linear Format, N = 5
        
        i2c_rd_ascii(1, addr, 0x99, 9+1,  "  MFR_ID: %s\n");
        i2c_rd_ascii(1, addr, 0x9A, 16+1, "  MFR_MODEL: %s\n");
        //i2c_rd_ascii(1, addr, 0x9B, 3+1,  "  MFR_REVISION: %s\n");
        //i2c_rd_ascii(1, addr, 0x9C, 2+1,  "  MFR_LOCATION: %s\n");
        //i2c_rd_ascii(1, addr, 0x9D, 4+1,  "  MFR_DATE: %s\n");
        i2c_rd_ascii(1, addr, 0x9E, 18+1, "  MFR_SERIAL: %s\n");
    }
        
    // disable I2C to power supplies
    hps_gpio_write(1, 23, 0); // (GPIO1_IO23 B28) atx_i2c_en

    return exit_code;
}

int check_dc_power(int addr, char *name_str, float voltage1, float voltage2, float *total_power, bool is_verbose) {
    
    int exit_code = 0;

    printf("[%d,0x%02X] %s ", 1, addr, name_str);

    float v1 = linear16_decode_paged(1, 0, addr, 0x8B, "V1: %0.3f V, "); // LTM4676A
    float v2 = linear16_decode_paged(1, 1, addr, 0x8B, "V2: %0.3f V, "); // LTM4676A
    
    float i1 = linear11_decode_paged(1, 0, addr, 0x8C, "I1:%6.3f A, "); // LTM4676A
    float i2 = linear11_decode_paged(1, 1, addr, 0x8C, "I2:%6.3f A, "); // LTM4676A
    
    float power = v1*i1 + v2*i2;
    printf("%2.0f W, ", power);
    *total_power += power;
    
    float t = linear11_decode(1, addr, 0x8E, "Temperature: "); // LTM4676A "Temperature: %0.1f°C"
    exit_code |= temperature_check("", t, 85.0f, false);

    return exit_code;
}

//-----------------------------------------------------------------------------

int check_fpga_die_temperatures() {
    
    char file_name_label[80];
    char file_name_input[80];
    char label[80];
    int  input;
    int exit_code = 0;
    
    for (int i=1; i <= 5; i++) {
        
        FILE *fp;
    
        // read label
        sprintf(file_name_label, "/sys/class/hwmon/hwmon0/temp%d_label", i);
        fp = fopen(file_name_label, "r");
        fscanf(fp, "%[^\n]", label);
        fclose(fp);
    
        // read temperature
        sprintf(file_name_input ,"/sys/class/hwmon/hwmon0/temp%d_input", i);
        fp = fopen(file_name_input, "r");
        fscanf(fp, "%d", &input);
        fclose(fp);
        
        printf("FPGA Temperature: ");
        
        // range 0 - 100
        exit_code |= temperature_check(label, input / 1000.0f, 100.0f, false);
    
    }

    return exit_code;
}

void voltage_check (char *label, float v, float min, float typ, float max) {
    
    int range = 5;
    int decimals;
    char format[80];
    
    if (typ < 10.0) {
        decimals = 2;
    } else {
        decimals = 1;
    }
    
    float v_step = (max - min) * 0.5f / (float)range;
    
    sprintf(format, "%%0.%dfV|", decimals);
    printf(format, min);
    for (int i = -range; i <= +range; i++) {
        if (typ+v_step*(i-0.5f) < v && v <= typ+v_step*(i+0.5f)) {
            printf("*");
        } else {
            printf("-");
        }
    }
    sprintf(format, "|%%0.%dfV %%0.%dfV %%s ", decimals, decimals+1);
    printf(format, max, v, label);
    
    if (v < min) {
        printf("Failure under voltage\n");
    } else if (v > max) {
        printf("Failure over voltage\n");
    } else if (v < min + 0.1*(max-min)) {
        printf("Warning close to minimum\n");
    } else if (v > max - 0.1*(max-min)) {
        printf("Warning close to maximum\n");
    } else {
        printf("Ok\n");
    }
    
}

void check_fpga_die_voltages() {
    
    char file_name_label[80];
    char file_name_input[80];
    char label[80];
    int  input;
    float voltage;
    
    for (int i=0; i <= 6; i++) {
    
        FILE *fp;
        
        // read label
        sprintf(file_name_label, "/sys/class/hwmon/hwmon0/in%d_label", i);
        fp = fopen(file_name_label, "r");
        fscanf(fp, "%[^\n]", label);
        fclose(fp);
    
        // read temperature
        sprintf(file_name_input ,"/sys/class/hwmon/hwmon0/in%d_input", i);
        fp = fopen(file_name_input, "r");
        fscanf(fp, "%d", &input);
        fclose(fp);
        
        printf("FPGA Power: ");
        
        if (i == 0) {
            voltage = input * 11.0f / 1000.0f;
        } else if (i == 1) {
            voltage = input * 3.0f / 1000.0f;
        } else {
            voltage = input / 1000.0f;
        }
        
        switch (i) {
            case 0: // FPGA 12V (1/11 ratio): 11.803V
                voltage_check(label, voltage, 11.0, 12.0, 13.0);
                break;
            case 1: // FPGA 2.5V (1/3 ratio): 2.493V
                voltage_check(label, voltage, 2.375, 2.5, 2.625);
                break;
            case 2: // FPGA 0.9V VCC: 0.899V
                voltage_check(label, voltage, 0.8-0.03, 0.9, 0.94+0.03);
                break;
            case 3: // FPGA 1.8V VCCIO: 1.825V
                voltage_check(label, voltage, 1.71, 1.8, 1.89);
                break;
            case 4: // FPGA 1.8V VCCPT: 1.818V
                voltage_check(label, voltage, 1.71, 1.8, 1.89);
                break;
            case 5: // FPGA 0.9V VCCERAM: 0.907V
                voltage_check(label, voltage, 0.87, 0.9, 0.93);
                break;
            case 6: // FPGA 1.8V VCCADC: 1.835V
                voltage_check(label, voltage, 1.71, 1.8, 1.89);
                break;
            default:
                break;
        }
    
    }
    
}

//-----------------------------------------------------------------------------

int main (int argc, char *argv[]) {
    
    signal(SIGINT, sigint_handler);
    
    int byte;
    bool is_v5_board;
    bool is_verbose = false; ///
    int exit_status = 0;
    float power = 0.0f;
    
    // check for verbose flag
    if (argc!=1) {
        if (strcmp("-v", argv[1])==0) {
            is_verbose = true;
        } else if (strcmp("-f", argv[1])==0) {
            flash_leds();
        } else {
            printf("Version: %s\n", VERSION);
            printf("Usage: ska-talondx-i2c-app [-v]\n");
            printf("  -v verbose\n");
            printf("  -f flash front panel LEDs (requires BIST bitstream)\n");
            exit(0);
        }
    }
    
    exit_status |= check_fpga_die_temperatures();

    check_fpga_die_voltages();
    
    printf("I²C Bus 0\n");
      
    // -------------
    // bus 0 devices

    // 0, 0x20 - Fan Controller
    exit_status |= check_fan_controller(is_verbose);
    
    // 0, 0x33 - ADC
    check_adc(is_verbose);
    
    // 0, 0x40 - Temperature & Humidity
    exit_status |= check_temperature_humidity(is_verbose);
    
    // 0, 0x50 - EEPROM
    check_eeprom(is_verbose);
    
    // 0, 0x43-0x47, 0x53-0x57 - MBOs
    exit_status |= check_mbos(is_verbose);

    // -------------
    // bus 1 devices
    
    // use power supply capability 0xB0 as check for bus 1 access
    byte = i2c_rd_byte(1, 0x40, 0x19);
    if (byte != 0xB0) {
        printf("I²C Bus 1 - Failed to access I²C bus (unable to detect I²C bus 1 power device), FPGA most likely not programmed\n");
        exit(0);
    }
    
    // check for v6 board only GPIO device
    byte = i2c_rd_byte(1, 0x21, 0x0);
    is_v5_board = (byte < 0) ? true : false; 
    if (is_v5_board) {
        printf("I²C Bus 1 - v5 board (v6 GPIO device not detected)\n");
    } else {
        printf("I²C Bus 1 - v6 board (v6 GPIO device detected)\n");
    }
    
    // 1, 0x21 (v6 only) GPIO
    i2c_rd_byte_check(1, 0x21, 0, "GPIO ", "Input: %02X\n");
    
    // 1, 0x54-0x57 DIMM SPD, 0x1C-0x1F DIMM Temperature
    exit_status |= check_dimm(is_verbose);

    // 1, 0x50 QSFP Modules
    check_qsfp(is_verbose);
    
    // GbE SFP Modules
    // 1, 0x50, 0x51 (v5)
    // 1, 0x72, 0x73 (v6)
    if (is_v5_board) {
        check_sfp(0x50);
    } else {
        check_sfp(0x72);
    }
    
    // 1, 0x59, 0x5B (v5): Power Supplies
    // 1, 0x6D, 0x6F (v6): Power Supplies
    if (is_v5_board) {
        // Note: not enabled due to access issue with v5 boards
        //check_ac_power(0x59, "AC Power Unit A", is_verbose);
        //check_ac_power(0x5B, "AC Power Unit B", is_verbose);
    } else {
        exit_status |= check_ac_power(0x6D, "AC Power Unit A (upper)", is_verbose);
        exit_status |= check_ac_power(0x6F, "AC Power Unit B (lower)", is_verbose);
    }
    
    // 1, 0x40,0x41,0x4A,0x4B DC Power converters
    exit_status |=  check_dc_power(0x40, "DC Power 3.3V     ", 3.30, 3.30, &power, is_verbose); // LTM4676A
    exit_status |=  check_dc_power(0x41, "DC Power 1.12/1.8V", 1.12, 1.80, &power, is_verbose); // LTM4677A
    exit_status |=  check_dc_power(0x4A, "DC Power 0.9/1.2V ", 0.90, 1.20, &power, is_verbose); // LTM4677A
    exit_status |=  check_dc_power(0x4B, "DC Power 1.12V    ", 1.12, 1.12, &power, is_verbose); // LTM4677A
    
    // 1, 0x42-0x49 DC Power converters with board mod
    // use power supply capability 0xB0 as check for PWRMGT 1 access mod
    byte = i2c_rd_byte(1, 0x42, 0x19);
    if (byte != 0xB0) {
        printf("Failed to detect 0.9V DC power device - board most likely missing I²C bus mod\n");
    } else {
        exit_status |=  check_dc_power(0x42, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x43, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x44, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x45, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x46, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x47, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x48, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        exit_status |=  check_dc_power(0x49, "DC Power 0.9V     ", 0.9, 0.9, &power, is_verbose); // LTM4676A
        if (is_verbose) {
            printf("  DC Total Power: %.0f W\n", power);
        }
    }

    return exit_status;
}
