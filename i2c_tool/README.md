# SKA Talon-DX I2C Application

## Overview

Application to test the two I2C busses on the Talon-DX board and query basic information from all devices to verify that they can be found and accessed.

I2C bus 0 devices can be accessed without the FPGA configured. I2C bus 1 devices require that the FPGA is programmed.

Note: Access to the AC power supply units is disabled in the application on v5 Talon-DX boards due to access issue.

## Usage

**To Display Options**

```
root@talon2:~# ./ska-talondx-i2c-app -h
Version: 1.0.0
Usage: ska-talondx-i2c-app [-v]
  -v verbose
```

**Example**

```
root@talon6:~# ./ska-talondx-i2c-app
FPGA Power: 11.0V|-----*-----|13.0V 11.96V 12V (1/11 ratio) Ok
FPGA Power: 2.38V|-----*-----|2.62V 2.502V 2.5V (1/3 ratio) Ok
FPGA Power: 0.77V|-------*---|0.97V 0.918V 0.8V VCC Ok
FPGA Power: 1.71V|--------*--|1.89V 1.852V 1.8V VCCIO Ok
FPGA Power: 1.71V|-------*---|1.89V 1.832V 1.8V VCCPT Ok
FPGA Power: 0.87V|--------*--|0.93V 0.915V 0.9V VCCERAM Ok
FPGA Power: 1.71V|--------*--|1.89V 1.856V 1.8V VCCADC Ok
I²C Bus 0
[0,0x20] Fan Controller, incorrect setting - fan speed not monitored
[0,0x33] ADC Register 0: F0
[0,0x40] Temperature: 29.7°C, Humidity: 24.1%
[0,0x50] EEPROM Ver: 0.0.1, PN: todo, Rev: todo, SN: P21_00015, Date: 2023-01-01, MAC: 40:A3:6B:50:00:3C
[0,0x56] Leap 1  Temperature: 20°C|-*---------|70°C 27.0°C  Ok
[0,0x54] Leap 2  Temperature: 20°C|--*--------|70°C 31.0°C  Ok
[0,0x53] Leap 3  Not Found
[0,0x55] Leap 4  Not Found
[0,0x57] Leap 5  Temperature: 20°C|-*---------|70°C 26.0°C  Ok
I²C Bus 1 - v5 board (v6 GPIO device not detected)
[1,0x21] GPIO  Not Found
[1,0x54] DIMM FO DDR4 SDRAM, RDIMM,  DDR4-2400 (1200 MHz clock),  16 GB / [1,0x1C] Temperature: 20°C|-*---------|85°C 25.8°C  Ok
[1,0x55] DIMM F1 DDR4 SDRAM, LRDIMM, DDR4-2666 (1333 MHz clock),  32 GB / [1,0x1D] Temperature: 20°C|-*---------|85°C 28.2°C  Ok
[1,0x56] DIMM F2 DDR4 SDRAM, LRDIMM, DDR4-2666 (1333 MHz clock),  32 GB / [1,0x1E] Temperature: 20°C|-*---------|85°C 26.8°C  Ok
[1,0x57] DIMM F3 DDR4 SDRAM, LRDIMM, DDR4-3200 (1600 MHz clock), 256 GB / [1,0x1F] Temperature: 20°C|---*-------|85°C 38.2°C  Ok
[1,0x50] 100G QSFP 0 Type: 100GBASE-CR4, Vendor: FS, PN: Q28-PC015, SN: S2113314623-1
[1,0x50] 100G QSFP 1 Type: 100GBASE-CR4, Vendor: FS, PN: Q28-PC015, SN: S2113314598-2
[9,0x50] GbE SFP 0 Vendor: FINISAR CORP., PN: FCLF8521P2BTL, SN: PW818D7
[8,0x50] GbE SFP 1 Not Found
[1,0x40] DC Power 3.3V      Voltage1: 3.300 V, Voltage2: 3.300 V, Temperature: 20°C|-----*-----|85°C 51.2°C  Ok
[1,0x41] DC Power 1.12/1.8V Voltage1: 1.125 V, Voltage2: 1.801 V, Temperature: 20°C|------*----|85°C 58.8°C  Ok
[1,0x4A] DC Power 0.9/1.2V  Voltage1: 0.900 V, Voltage2: 1.201 V, Temperature: 20°C|------*----|85°C 56.6°C  Ok
[1,0x4B] DC Power 1.12V     Voltage1: 1.123 V, Voltage2: 1.125 V, Temperature: 20°C|-----*-----|85°C 53.4°C  Ok
[1,0x42] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.910 V, Temperature: 20°C|----*------|85°C 47.4°C  Ok
[1,0x43] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.911 V, Temperature: 20°C|-----*-----|85°C 51.9°C  Ok
[1,0x44] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.910 V, Temperature: 20°C|-----*-----|85°C 49.7°C  Ok
[1,0x45] DC Power 0.85V     Voltage1: 0.907 V, Voltage2: 0.911 V, Temperature: 20°C|----*------|85°C 47.8°C  Ok
[1,0x46] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.911 V, Temperature: 20°C|-----*-----|85°C 53.6°C  Ok
[1,0x47] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.910 V, Temperature: 20°C|----*------|85°C 44.4°C  Ok
[1,0x48] DC Power 0.85V     Voltage1: 0.906 V, Voltage2: 0.910 V, Temperature: 20°C|-----*-----|85°C 50.6°C  Ok
[1,0x49] DC Power 0.85V     Voltage1: 0.907 V, Voltage2: 0.910 V, Temperature: 20°C|----*------|85°C 46.5°C  Ok
FPGA Temperature: 20°C|--*--------|100°C 38.0°C Main Die SDM Ok
FPGA Temperature: 20°C|-*---------|100°C 30.7°C Tile Bottom Left Ok
FPGA Temperature: 20°C|--*--------|100°C 38.9°C Tile Top Left Ok
FPGA Temperature: 20°C|--*--------|100°C 36.2°C Tile Bottom Right Ok
FPGA Temperature: 20°C|--*--------|100°C 36.8°C Tile Top Right Ok
```
