#!/bin/sh

# path of the BIST source files
BIST_SRC_PATH="/home/root/packages/bist"
# list of BIST source files
BIST_SRC_FILES="talon_dx-tdc_base-tdc_bist.tar.gz tdc.ipmap"
# path of the BIST systemd service
BIST_SERVICE_PATH="/etc/systemd/system"
# list of systemd service files
BIST_SERVICE_FILES="bist.service bist.timer"
# path of the BIST bitstream archive
BIST_ARCHIVE=$BIST_SRC_PATH/talon_dx-tdc_base-tdc_bist.tar.gz
# path of the BIST bitstream for programming to trigger the overlay
BIST_BITSTREAM_PATH="/sys/kernel/config/device-tree/overlays"
# path of the BIST bitstream package is extracted
BIST_ARCHIVE_PATH=$BIST_SRC_PATH/extract_dir

usage() {
    echo "Usage:"
    echo "-i             Get info on the status of bist (running, waiting, active, etc)"
    echo "-s             Start the BIST systemd service and extract tar file"
    echo "-k             Kill the BIST systemd service immediately, aborting the BIST"
    echo "-r             Run the BIST immediately, without enabling the systemd timer"
    echo "-m <time>      Modify the BIST systemd start delay time by <time>"
    echo "-t             Show the current BIST systemd start delay time"
    echo "-p             Program the BIST bitstream"
    echo "-x             Extract the tar file at pre-defined location"
    echo "-f             Publish the results (.csv) of the BIST to influxdb"
    echo "-c             Print the results (.txt) of the BIST"
    echo "-v             Verify the BIST files are installed correctly"
    echo "-h             Display this help message"

    echo "Some helpful commands:
        systemctl status bist.timer
        systemctl status bist.service
        journalctl -u bist.service
        systemctl list-timers bist.timer
        systemctl is-enabled bist.timer
        systemctl is-active bist.timer
        systemctl show --property MainPID --value bist.service
    "
}

verify_bist_service_files() {
    # verify that the required systemd service files are placed at right location
    res=0
    verbosity=$1
    for file in $BIST_SERVICE_FILES; do
        if ! [ -f $BIST_SERVICE_PATH/"$file" ]; then
            if [ "$verbosity" ]; then
                echo "$BIST_SERVICE_PATH/$file not found"
            fi
            res=1
        fi
    done
    return $res
}

verify_bist_bitstream_files() {
    # verify that the required bitstream files are placed at right location
    res=0
    verbosity=$1
    for file in $BIST_SRC_FILES; do
        if ! [ -f $BIST_SRC_PATH/"$file" ]; then
            if [ "$verbosity" ]; then
                echo "$BIST_SRC_PATH/$file not found"
            fi
            res=1
        fi
    done
    return $res
}

verify_bist_files() {
    # verify all bist files dependencies
    verbosity=$1
    res=0
    if ! verify_bist_service_files "$verbosity"; then
        res=1
    fi
    if ! verify_bist_bitstream_files "$verbosity"; then
        res=1
    fi
    return $res
}

verify_bist_service() {
    res=$(systemctl is-active $BIST_SERVICE_PATH/bist.timer)
    echo "$res"
}

start_bist_service() {
    echo "Activating BIST service via systemd"
    systemctl enable $BIST_SERVICE_PATH/bist.timer
    return $?
}

stop_bist_service() {
    echo "Stopping BIST service via systemd"
    systemctl disable --now $BIST_SERVICE_PATH/bist.timer #disable and stop immediately
    return $?
}

extract_archive() { 
    # tar command on the Talon boards is from Busybox and 
    # has a limited set of command. tar options such as --get and --wildcards 
    # does not exist.
    echo "Extracting files from $BIST_ARCHIVE to $BIST_ARCHIVE_PATH"
    if [ -f $BIST_ARCHIVE ]; then

        # remove the output directory if it exists
        if [ -d "$BIST_ARCHIVE_PATH" ]; then
            echo "Deleting previous tar file output directory"
            rm -rf $BIST_ARCHIVE_PATH
        fi

        mkdir $BIST_ARCHIVE_PATH
        tar -xvzf $BIST_ARCHIVE -C $BIST_ARCHIVE_PATH

    else
        echo "$BIST_ARCHIVE not found, aborting..."
        exit 3
    fi
}

program_bist_bitstream() {
    echo "Programming the BIST bitstream..."
    # fetch the files
    bs_core=$(ls $BIST_ARCHIVE_PATH/*.rbf) 
    dtb=$(ls $BIST_ARCHIVE_PATH/*.dtb)
    # sanity check
    if [ -z "$dtb" ] || [ -z "$bs_core" ]; then
        echo ".dtb or .rbf file(s) missing. Aborting bitstream programming!"
        exit 3
    else
        echo "Using $bs_core and $dtb"
        rmdir $BIST_BITSTREAM_PATH/*
        mkdir $BIST_BITSTREAM_PATH/base
        # copy the files over to the tempfs 
        cp "$dtb" /lib/firmware
        cp "$bs_core" /lib/firmware
        # grab the basename of the target files
        target_dtb=$(basename "$dtb")
        #target_bs_core=$(basename "$bs_core")
        # move to the directory in a new shell and write the overlay
        (cd /lib/firmware && echo "$target_dtb" > $BIST_BITSTREAM_PATH/base/path)
        dmesg | tail -n 10
    fi
}

execute_bist() {
    echo "Executing BIST..."
    # fetch the files
    json=$(ls $BIST_SRC_PATH/extract_dir/*.json)
    ipmap=$(ls $BIST_SRC_PATH/*ipmap)
    # sanity check
    if [ -z "$json" ] || [ -z "$ipmap" ]; then
        echo "ipmap or json file(s) missing. Aborting BIST execution!"
        exit 1
    else
        echo "Using $json and $ipmap"
        # the two parentheses enclosing the command mean it runs in a new shell
        (cd $BIST_SRC_PATH/src; python3 $BIST_SRC_PATH/src/run_bist_tests.py "$json" "$ipmap")
    fi
}

run_bist() {
    verify_bist_bitstream_files true
    res=$?
    if [ $res -eq 0 ]; then
        program_bist_bitstream
        execute_bist
    fi
    return $res
}

get_bist_results() {
    # print the results of the BIST
    if [ -f $BIST_SRC_PATH/src/tdc_base_bist_logfile.txt ]; then
        less $BIST_SRC_PATH/src/tdc_base_bist_logfile.txt
        return 0
    else
        echo "$BIST_SRC_PATH/src/tdc_base_bist_logfile.txt not found"
        exit 1
    fi
}

publish_bist_results_to_influxdb() {
    # publish the results to the influxdb. The python script that runs
    # the BIST is responsible for producing a valid .csv file
    if [ -f $BIST_SRC_PATH/src/tdc_base_bist_logfile.csv ]; then
        echo "Publishing $BIST_SRC_PATH/src/tdc_base_bist_logfile.csv to influxdb"
        influx write --bucket bist --file $BIST_SRC_PATH/src/tdc_base_bist_logfile.csv
        return 0
    else 
        echo "$BIST_SRC_PATH/src/tdc_base_bist_logfile.csv not found."
        exit 1
    fi
}

get_bist_status(){

    if ! verify_bist_service_files true; then
        #we return zero here because if the checks have failed
        #then the bist is not going to run, therefore it would
        #be safe to continue with reprogramming the board if
        #this script is used to check the status of bist
        return 0
    fi

    timer_en=$(systemctl is-enabled bist.timer)
    timer_act=$(systemctl is-active bist.timer)
    bist_act=$(systemctl is-active bist.service)
    timer_trig=$(systemctl status bist.timer | grep "Trigger:")
    timer_stat=$(systemctl status bist.timer | grep "Active:")
    
    if [ "$timer_en" = "enabled" ] && [ "$timer_act" = "active" ]; then
        if [ "$bist_act" = "activating" ]; then
            bist_pid=$(systemctl show --property MainPID --value bist.service)
            echo "bist.service is running, PID=$bist_pid"
            return 1
        elif [ "$bist_act" = "inactive" ] && [ "$timer_trig" = "    Trigger: n/a" ]; then
            echo "bist has already executed"
            echo "$timer_stat"
            return 0
        elif [ "$bist_act" = "inactive" ]; then
            echo "bist is awaiting execution"
            echo "$timer_trig"
            return 2
        fi
    else
        echo "bist.timer (and therefore bist.service) is disabled"
        return 0
    fi
}

get_bist_start_delay() { 
    if verify_bist_service_files true; then
        val=$(cat $BIST_SERVICE_PATH/bist.timer | grep seconds)
        echo "$val"
    fi
}

modify_bist_start_delay() {
    if verify_bist_service_files true; then
        echo "Setting the BIST start-up delay to ${1} seconds"
        sed -i "/OnBootSec=/c\OnBootSec=${1}seconds" $BIST_SERVICE_PATH/bist.timer
    else
        exit 1
    fi
}

create_influxdb_bist_bucket(){
    influx bucket create --name bist -r 7d --description "bucket for storing the results of the BIST"
    return $?
}

is_integer(){
    variable=$1
    case ${variable#[-+]} in
    *[!0-9]* | '') return 1 ;;
    * ) return 0 ;;
    esac
}

while getopts ":hskrm:vtpxfci" arg; do
    case $arg in
        s)
            if ! start_bist_service; then
                echo "Error starting the BIST service"
                exit 5
            fi
            # also extract the bitstream
            extract_archive
            ;;
        k)
            
            if ! stop_bist_service; then
                echo "Error killing the BIST service"
                exit 4
            fi
            ;;
        r)
            run_bist
            ;;
        t)
            get_bist_start_delay
            ;;
        x)
            extract_archive
            ;;
        m)
            start_delay=${OPTARG}
            if ! is_integer "$start_delay"; then #check if the argument passed is only digits
                echo "delay has to be a positive integer in seconds"
            else
                modify_bist_start_delay "$start_delay"
                get_bist_start_delay
            fi
            ;;
        v)
            
            if ! verify_bist_files true; then
                exit 1
            fi

            
            if ! influx bucket ls --name bist; then
                echo "Creating the bist bucket..."
                create_influxdb_bist_bucket
            fi
            
            ;;
        p)
            program_bist_bitstream
            ;;
        c)
            get_bist_results
            ;;
        f)
            publish_bist_results_to_influxdb
            ;;
        i)
            get_bist_status
            return $?
            ;;
        h)
            usage
            ;;
        :)
            echo "Error: -${OPTARG} requires an argument."
            ;;
        *)
            usage
            ;;
    esac
done

# In case no options were passed
if [ $OPTIND -eq 1 ]; then usage; fi