#!/usr/bin/env python3

import sys
import os
import subprocess
import shutil
import logging

logging.basicConfig(level=logging.DEBUG)

class TestRequirements():
    """Test availability of required packages."""
    def __init__(self, target_board, requirement_file):
        self.board = target_board
        self.req_file = requirement_file
        self.URLS = {
            "scipy": "https://github.com/scipy/scipy/releases/download/v1.11.3/scipy-1.11.3-cp310-cp310-manylinux_2_17_aarch64.manylinux2014_aarch64.whl",
        }
        
    def append_custom_pkg(self):
        if len(self.URLS.keys()) == 0:
            return
        logging.info(f"Appending custom packages to {self.req_file}...")
        with open(self.req_file, "a") as file:
            for pkg, _ in self.URLS.items():
                #FIXME:this will append the item multiple times if the same file is used, but it works regardless 
                file.write(f"\n{pkg}")
                    
    def bootstrap_pip_remote(self):
        logging.info(f"Bootstrapping pip on {self.board}...")
        cmd = f'ssh root@{self.board} python3 -m ensurepip'
        try:
            sub = subprocess.run(cmd, capture_output=True, shell=True, check=True, timeout=40)
        except Exception as e:
            logging.warning(f'exception: {e}')
        else:
            logging.warning(f"error code: {sub.returncode}")
            logging.warning(sub.stdout.decode())

    def download_python_packages(self):
        logging.info(f"Downloading packages...")
        if os.path.exists('./req_downloads'):
            logging.info("Deleting the req_downloads directory...")
            shutil.rmtree('./req_downloads')

        cmd = f'python3 -m pip download --dest req_downloads -r {self.req_file}'
        try:
            sub = subprocess.run(cmd, capture_output=True, shell=True, check=True, timeout=40)
        except Exception as e:
            logging.warning(f'exception: {e}')
        else:
            logging.warning(f"error code: {sub.returncode}")
            logging.warning(sub.stdout.decode())
            
        for pkg, url in self.URLS.items():
            logging.info(f"Downloading {pkg}")
            cmd = f'wget {url} --directory-prefix=req_downloads'
            try:
                sub = subprocess.run(cmd, capture_output=True, shell=True, check=True, timeout=40)
            except Exception as e:
                logging.warning(f'exception: {e}')
            else:
                logging.info(f"error code: {sub.returncode}")
                logging.info(sub.stdout.decode())

    def transfer_package_remote(self):
        logging.info(f"transferring packages across...")
        if os.path.exists('./req_downloads'):
            commands = [
                f'scp -r req_downloads root@{self.board}:/home/root/packages',
                f'scp {self.req_file} root@{self.board}:/home/root/packages',
            ]
            for cmd in commands:
                try:
                    sub = subprocess.run(cmd, capture_output=True, shell=True, check=True, timeout=40)
                except Exception as e:
                    logging.warning(f'exception: {e}')
                else:
                    logging.info(f"error code: {sub.returncode}")
                    logging.info(sub.stdout.decode())
        else:
            logging.warning("req_downloads directory does not exist!")

    def install_packages_remote(self):
        logging.info(f"installing packages remotely...")
        cmd = f"ssh root@{self.board} python3 -m pip install --no-index --find-links=/home/root/packages/req_downloads -r /home/root/packages/{self.req_file}"
        try:
            sub = subprocess.run(cmd, capture_output=True, shell=True, check=True, timeout=40)
        except Exception as e:
            logging.warning(f'exception: {e}')
        else:
            logging.warning(f"error code: {sub.returncode}")
            logging.warning(sub.stdout.decode())


def main():
    if len(sys.argv) == 3:
        board=sys.argv[1]
        req_file=sys.argv[2]
        tester = TestRequirements(board, req_file)
        tester.download_python_packages()
        tester.bootstrap_pip_remote()
        tester.append_custom_pkg()
        tester.transfer_package_remote()
        tester.install_packages_remote()
    else:
        print("python3 pkg_download.py TALON_BOARD REQUIREMENTS.TXT")

if __name__ == "__main__":
    main()
